# -*- coding: utf-8 -*-

import os, sys
import logging

from tensorflow.python.summary import event_multiplexer
from tensorflow.tensorboard.backend import application
from tensorflow.tensorboard.plugins.projector import plugin as projector_plugin
 
logger = logging.getLogger(__name__)


def app(logdir, purge_orphaned_data=True, reload_interval=60):
    logger.debug("TensorBoardRpc open called")
    
    logdir = os.path.expandvars(logdir)

    multiplexer = event_multiplexer.EventMultiplexer(
        size_guidance=application.DEFAULT_SIZE_GUIDANCE,
        purge_orphaned_data=purge_orphaned_data)
    plugins = {'projector': projector_plugin.ProjectorPlugin()}
    a = application.TensorBoardWSGIApp(
        logdir,
        plugins,
        multiplexer,
        reload_interval=reload_interval)
    
    def _wrapper(env_keys, env_values, start_response):
        return a(dict(zip(env_keys, env_values)), start_response)
    
    return _wrapper