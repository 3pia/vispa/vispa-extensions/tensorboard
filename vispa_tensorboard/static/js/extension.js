define(["vispa/extensions",  "vispa/views/center", "jquery", "css!../css/style"],
       function(Extensions,  CenterView, $, x) {

	var TensorBoardExtension = Extensions.Extension._extend({

	// the constructor
    init: function init() {
      // call the super constructor
      init._super.call(this, "tensorboard");

      this.addView("tensorboard", TensorBoardView);

      var self = this;

      // add default preferences for the DemoView
      // the keys (e.g. backgroundColor) are used as the preference keys
      // within the instance
      // there are more types than just strings:
      // string, integer, float, boolean, list, object
      // all of them can have a predefined selection
      // intergers and floats also can have a range, consisting of a list of 2
      // parameters (upper and lower bound)
//      this.setDefaultPreferences(DemoView, {
//        backgroundColor: {
//          // required
//          type: "string",
//          value: "blue",
//          // optional
//          description: "The background color",
//          selection: ["#428bca", "#f0ad4e", "#d9534f", "#5cb85c"]
//        }
//      }, {
//        title: "Demo"
//      });

      // now we add default shortcuts
      // the structure of the second argument is the same as above in 
      // "setDefaultPreferences" except for the missing range/selection entries
      // and the additional callback entry
      // the callback will have the scope of the executing view instance!
//      this.setDefaultShortcuts(DemoView, {
//        test: {
//          description: "some test shortcut",
//          value: vispa.device.isMac ? "meta+s" : "ctrl+s",
//          callback: function() {
//            this.setModified(false);
//            this.alert("Instance " + this.getId() + " saved!");
//          }
//        }
//      }, {
//        title: "Demo"
//      });

      // set options for the DemoView
//      this.setOptions(DemoView, {
//        maxInstances: 5
//      });

      // add a menu entry to the main menu
      // there is one main menu per workspace
      // "addMenuEntry" takes two parameters, a title and a callback
      // the second parameter can also be an object for more configuration
      // submenus are not supported (yet)
      this.addMenuEntry("Open TensorBoard (Alpha Version)", {
        // a bootstrap icon class
        iconClass: "glyphicon glyphicon-plus",
        // the callback that is fired when the menu entry is clicked
        // the id of the workspace this menu entry is subject to is passed
        callback: function(workspaceId) {
          // we want to create a new DemoView instance using the "createInstance"
          // method implemented in the extension class
          // this method requires the workspace id as the first and a reference to
          // the target view class as the second argument
          // any additional arguments (starting from index 2) are passed to the
          // constructor of the references view class
          // see its description for further information
          self.createInstance(workspaceId, TensorBoardView);
        }
      });
    }
  });


  // an extension can have multiple views, i.e., multiple (different)
  // representations of content delivered by the extension
  // example: a file browser might have a large (center) view and a smaller one
  // to show a file tree
  // in this case, we define a view that inherits from the center view base class
  // definition in vispa.extensionView.Center
  // other views are not supported yet
  var TensorBoardView = CenterView._extend({
    //TODO: modify text below
    // the constructor
    // the first argument is an object containing synchronized preferences
    // for all instances of this extension view which can also be synchronized
    // with the preference database (see below)
    // the exact content of the preferences depends on the defaults set for this
    // view in the extension above (in this case: {backgroundColor: "blue"})
    // depending on the way this instance is created, there might be additional
    // arguments (starting from index 2)
    //***
    init: function init() {

      //TODO: modify text below
      // call the super constructor with 2 parameters
      // 1. the name of this view (characters, numbers, and underscores only)
      // 2. the preference object
      // after this call, the preferences are automatically available via
      // this.getPreference(key) and this.setPreference(key, value)
      //***
      init._super.apply(this, arguments);

      // attributes like e.g. the id are unknown at this point as they are set
      // right after the init function
      // make sure to move all initial operations that require attributes are
      // placed in the setup function or wrapped in callbacks (e.g. actions of
      // menu entries)

      // the content node
      this.node = null;

      // add a menu entry to the instance's menu
      // there is one menu per instance
      // "ExtensionView.addMenuEntry" implements the same syntax as
      // "Extension.addMenuEntry" (see above)
      // this time, we simply pass a callback as the second argument
      var self = this;
      //this.addMenuEntry("My ID", function() {
        // alert the id of this instance
        // we don't use the browser's default alert function since each instance
        // has its own context (messages are shown when the instance is shown)
        // all basic message functions are available: alert, prompt, and confirm
        //self.alert("My ID is: " + self.getId());
      //});

      // set the label
      this.setLabel("id: " + this.getId());     
    },

    // applyPreferences is called when there was a change to the preferences
    applyPreferences: function applyPreferences() {
      applyPreferences._super.call(this);

      // backgroundColor
      this.node.css("background-color", this.getPreference("backgroundColor"));
    },

    // implement the render function
    // the passed node can be filled with content
    // you can store the node to use it for later actions outside of the render function
    render: function(node) {
      var view = this;

      // we store the node
      this.node = node;

      // set the icon
      this.setIcon("fa fa-play-circle-o");

   
      //file selection with selector
      var args = {
        "path": "$HOME",
        callback: function(path) {
          //Add a label to the view's tab
    	  view.setLabel("TensorBoard: " + path);
          view.GET("open?logdir="+path, function(err, tid) {
            view.tid = tid;
            var url = view.getExtension().dynamic(tid);
            $("<iframe class=\"tensorboard\" src=\""+url+"/\" />").appendTo(node);
          });
        },
        sort: "name",
        reverse: false,
        foldermode: true
      };
      view.spawnInstance("file", "FileSelector", args);
    },

    // called when the instance is resized
    onResize: function(dimensions) {
      // dimensions contains the new height and width
    },

    // the following methods are fired before and after certain events
    // return false in the "before" methods to prevent the event
    onFocus: function() {
      return this;
    },
    onBlur: function() {
      return this;
    },
    onBeforeClose: function() {
    	this.GET("close", {tid: this.tid});
      return this;
    }
  });

  return TensorBoardExtension;
});