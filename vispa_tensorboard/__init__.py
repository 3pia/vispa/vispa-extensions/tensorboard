# -*- coding: utf-8 -*-

from vispa.server import AbstractExtension
from vispa.controller import AbstractController
from vispa import workspace, url
import cherrypy
import uuid

# TODO: remove app when workspace closes
# TODO: add user auth midware
 
class TensorBoardController(AbstractController):

    @cherrypy.expose
    def open(self, logdir):
        self.release_session()
        viewId = cherrypy.request.private_params.get("_viewId", None)
        tid = viewId + "-" + str(uuid.uuid4())
        m = workspace.module("vispa.extensions.tensorboard.workspace")
        a = m.app(logdir)
        script_name = self.script_name(tid)
        def _w(environ, start_response):
            return a(tuple(environ.keys()), tuple(environ.values()), start_response)
        cherrypy.tree.graft(_w, script_name)

        return tid

    @cherrypy.expose
    def close(self, tid):
        script_name = self.script_name(tid)
        if script_name in cherrypy.tree.apps:
            del cherrypy.tree.apps[script_name]

    def script_name(self, tid):
        return url.dynamic("extensions", "tensorboard", tid, encoding='utf-8')
        
class TensorBoardExtension(AbstractExtension):

    def name(self):
        return 'tensorboard'

    def dependencies(self):
        return []

    def setup(self):
        self.add_controller(TensorBoardController())
        self.add_workspace_directoy()
