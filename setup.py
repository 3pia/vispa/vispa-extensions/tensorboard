#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name             = "vispa_tensorboard",
    version          = "0.0.1",
    description      = "VISPA TensorBoard - Display TensorFlow logs.",
    author           = "VISPA Project",
    author_email     = "vispa@lists.rwth-aachen.de",
    url              = "http://vispa.physik.rwth-aachen.de/",
    license          = "GNU GPL v2",
    packages         = ["vispa_tensorboard"],
    package_dir      = {"vispa_tensorboard": "vispa_tensorboard"},
    package_data     = {"vispa_tensorboard": [
        "static/css/*",
        "static/html/*",
        "static/js/*",
    ]},
)
